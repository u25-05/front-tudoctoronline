//import logo from './logo.svg';
//import './App.css';
import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "./paginas/auth/Login";
import CrearCuenta from "./paginas/auth/CrearCuenta";
import Home from "./paginas/Home";
/* import CitasAdmin from "../src/paginas/citas/CitasAdmin"
import CitasCrear from "../src/paginas/citas/CitasCrear"
import CitasEditar from "../src/paginas/citas/CitasEditar" */
/* import TratamientoAdmin from "./paginas/tratamiento/TratamientoAdmin";
import TratamientoCrear from "./paginas/tratamiento/TratamientoCrear";
import TratamientoEditar from "./paginas/tratamiento/TratamientoEditar";
import PersonasEditar from "./paginas/personas/PersonasEditar";
import PersonasAdmin from "./paginas/personas/PersonasAdmin";
import PersonasCrear from "./paginas/personas/PersonasCrear"; */

function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login></Login>}></Route>
          <Route path="/crear-cuenta" exact element={<CrearCuenta></CrearCuenta>}></Route>
          <Route path="/home" exact element={<Home></Home>}></Route>
       {/*    <Route path="/citas-admin" exact element={<CitasAdmin></CitasAdmin>}></Route>
          <Route path="/citas-crear" exact element={<CitasCrear></CitasCrear>}></Route>
          <Route path="/citas-editar/:idCita" exact element={<CitasEditar></CitasEditar>}></Route> */}
          {/* <Route path="/tratamientos-admin" exact element={<TratamientoAdmin></TratamientoAdmin>}></Route>
          <Route path="/tratamiento-crear" exact element={<TratamientoCrear></TratamientoCrear>}></Route>
          <Route path="/tratamiento-editar/:idTratamiento" exact element={<TratamientoEditar></TratamientoEditar>}></Route>
          <Route path="/personas-admin" exact element={<PersonasAdmin></PersonasAdmin>}></Route>
          <Route path="/personas-crear" exact element={<PersonasCrear></PersonasCrear>}></Route>
          <Route path="/personas-editar/:idPersona" exact element={<PersonasEditar></PersonasEditar>}></Route> */}
        </Routes>
      </Router>
    </Fragment>
  );
}
export default App;
