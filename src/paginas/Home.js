import React from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../componentes/ContentHeader";
import Footer from "../componentes/Footer";
import Navbar from "../componentes/Navbar";
import SidebarContainer from "../componentes/SidebarContainer";

const Home = () => {
    /* esto */
  return (
    <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
        titulo={"Tu Doctor Online"}
        breadCrumb1={"Inicio"}
        breadCrumb2={"Tu Doctor Online"}
        breadCrumb3={"Tratamientos"}
        breadCrumb4={"Personas"}
        ruta1={"/home"}
        ruta3={"/tratamientos-admin"}
        ruta4={"/personas-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
        <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-3 col-6">
                        <div className="small-box bg" style={{ background: "#2E7398", color: 'white'}}>
                                <div className="inner">
                                    <h3>Citas </h3>
                                    <p>&nbsp;</p>
                                </div>
                                <div className="icon">
                                    <i className="ion fa fa-edit"></i>
                                </div>
                                <Link to ={"/citas-admin"} className="small-box-footer">
                                    Citas
                                    <i className="fas fa-arrow-circle-right"></i>
                                </Link>
                            </div>
                        </div>
                        <div className="col-lg-3 col-6">
                        <div className="small-box bg" style={{ background: "#155678" }}>
                                <div className="inner">
                                  <h3 style={{color: 'white'}} >Tratamientos</h3>
                                    <p>&nbsp;</p>
                                </div>
                                <div className="icon">
                                    <i className="ion fa fa-edit"></i>
                                </div>
                                <Link to ={"/tratamientos-admin"} className="small-box-footer">
                                    Tratamientos
                                    <i className="fas fa-arrow-circle-right"></i>
                                </Link>
                            </div>
                        </div>
                        <div className="col-lg-3 col-6">
                        <div className="small-box bg" style={{ background: "#ffab40" }}>
                                <div className="inner">
                                    <h3 style={{color: 'white'}}>Personas</h3>
                                    <p>&nbsp;</p>
                                </div>
                                <div className="icon">
                                    <i className="ion fa fa-edit"></i>
                                </div>
                                <Link to ={"/personas-admin"} className="small-box-footer">
                                    Personas
                                    <i className="fas fa-arrow-circle-right"></i>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        </div>
        <Footer></Footer>
    </div>
  );
};

export default Home;