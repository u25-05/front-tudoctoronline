import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert';

const CitasAdmin = () => {

    const [citas, setCitas] = useState([]);

    const cargarCitas = async () => {
        const response = await APIInvoke.invokeGET(`/api/citas`);
        console.log('pendiente', response)
        setCitas(response.citas)
    }

    useEffect (() => {
        cargarCitas();
    },[])

    const eliminarCitas = async (e, idCitas) =>{
      e.preventDefault();
      
      const response = await APIInvoke.invokeDELETE(`/api/citas/${idCitas}`);
    
      if (response.msg === 'Cita eliminada'){
        const msg = "Esta cita ha sido eliminado correctamente"
        swal({
          title: 'Informacion',
          text: msg,
          icon: 'success',
          buttons:{
            confirm:{
            text: 'Ok',
            value: true,
            visible: true,
            className: 'btn btn-primary',
            closeModal: true}
          }
        })
        cargarCitas();
      }
      else{
        const msg = "Esta cita NO ha sido eliminada correctamente"
        swal({
          title: 'Error',
          text: msg,
          icon: 'error',
          buttons:{
            confirm:{
            text: 'Ok',
            value: true,
            visible: true,
            className: 'btn btn-danger',
            closeModal: true}
          }
        })
       
      }

    }


  return (
    <div className="wrapper">
      <Navbar></Navbar>
     <SidebarContainer></SidebarContainer> 
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Listado de Citas"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Citas"}
          breadCrumb3={"Tratamientos"}
          breadCrumb4={"Personas"}

          ruta1={"/home"}
          ruta3={"/tratamientos-admin"}
          ruta4={"/personas-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
          <div className="card">
            <div className="card-header">
              <h3 className="card-title">
                {/* <button type="button" > */}
                    <Link to ={"/citas-crear"}className="btn-block btn-primary btn-sm">
                        Crear
                    </Link>
                    
                    {/* </button> */}
              </h3>
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            {/* 
              
                <div className="card-header">
                  <h3 className="card-title">Bordered Table</h3>
                </div> */}
                {/* /.card-header */}
                <div className="card-body">
                  <table className="table table-bordered">
                    <thead>
                      <tr>
                        <th style={{ width: '10%' }}>ID</th>
                        <th style={{ width: '70%' }}>Nombre</th>
                        <th style={{ width: "20%" }}>Acciones</th>                       
                      </tr>
                    </thead>
                    <tbody>
                        {citas.map(
                            item =>
                            <tr key={item._id}>
                                <td key={item._id}>{item._id}</td>
                                <td key={item.nombre}>{item.nombre} &nbsp;</td>
                                <td>
                                    <Link to={`/citas-editar/${item._id}@${item.nombre}`} className="btn btn-sm btn-primary" >Editar</Link>
                                    &nbsp;
                                    <button onClick={(e)=> eliminarCitas(e,item._id)} 
                                    className="btn btn-sm btn-danger" >Borrar</button>
                                    &nbsp;
                               
                                </td>                           
                          </tr>
                        )}
                     
                  
                    </tbody>
                  </table>
                </div>
                {/* /.card-body */}
                <div className="card-footer clearfix">
                  <ul className="pagination pagination-sm m-0 float-right">
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        «
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        1
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        2
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        3
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        »
                      </Link>
                    </li>
                  </ul>
                </div>
              
            

            {/* /.card-body */}
            {/* <div className="card-footer">Footer</div> */}
            {/* /.card-footer*/}
          </div>
        </section>
      </div>
     <Footer></Footer>
    </div>
  );
};

export default CitasAdmin;
