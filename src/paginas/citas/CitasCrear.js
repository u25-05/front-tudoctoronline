import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Navbar from "../../componentes/Navbar";
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";
import SidebarContainer from "../../componentes/SidebarContainer";
import Footer from "../../componentes/Footer";

const CitasCrear = () => {

  const navigate = useNavigate();

  const [cita, setCitas] = useState({
    nombre:''
  });

  const { nombre } = cita;

  useEffect(() =>{
    document.getElementById("nombre").focus()
  })

  const onChange = (e) => {
    setCitas({
      ...cita,
      [e.target.name]:e.target.value
    })
  }

  const crearCita = async() => {
    const data = {
      nombre: cita.nombre
    }

    const response = await APIInvoke.invokePOST(`/api/citas`, data);
    
    console.log("Response",response)
    
const idCita = response._id;

    if (idCita === ''){
      const msg = "No se creo ningun cita"
      swal({
        title: 'Error',
        text: msg,
        icon: 'error',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-danger',
          closeModal: true}
        }
      })
    }else{
      navigate("/citas-admin")
      const msg = "Esta cita fue creada correctamente"
      swal({
        title: 'Informacion',
        text: msg,
        icon: 'success',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-primary',
          closeModal: true}
        }
      })
      setCitas({nombre:''})
    }
  }

  
  const onSubmit = (e) => {
    e.preventDefault();
    console.log("onsubmit", e)
    crearCita();
  }

    return(
        <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Creacion Citas"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Citas"}
          breadCrumb3={"Tratamientos"}
          breadCrumb4={"Personas"}

          ruta1={"/home"}
          ruta3={"/tratamientos-admin"}
          ruta4={"/personas-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
          <div className="card">
            <div className="card-header">
             
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            {/* 
              
                <div className="card-header">
                  <h3 className="card-title">Bordered Table</h3>
                </div> */}
                {/* /.card-header */}
                <div className="card-body">
                    <form onSubmit={onSubmit}>
                        <div className="card-body">
                            <div className="form-group">
                                <label htmlFor="nombre">Tipo</label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" placeholder="Escriba el nombre del cita"
                                value={nombre} onChange ={onChange} required>
                                </input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="nombre">Fecha</label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" /* placeholder="Escriba el nombre del cita"
                                value={nombre} onChange ={onChange} required */>
                                </input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="nombre">Id del Médico</label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" /* placeholder="Escriba el nombre del cita"
                                value={nombre} onChange ={onChange} required */>
                                </input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre del Paciente</label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" /* placeholder="Escriba el nombre del cita"
                                value={nombre} onChange ={onChange} required */>
                                </input>
                            </div>
                        </div>
                        <div>
                          <button type="submit" className="btn btn-primary">
                            Crear
                          </button>
                        </div>
                    </form>
                </div>
                {/* /.card-body */}
                <div className="card-footer clearfix">
                  <ul className="pagination pagination-sm m-0 float-right">
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        «
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        1
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        2
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        3
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        »
                      </Link>
                    </li>
                  </ul>
                </div>
              
            

            {/* /.card-body */}
            {/* <div className="card-footer">Footer</div> */}
            {/* /.card-footer*/}
          </div>
        </section>
      </div>
      <Footer></Footer>
    </div>
    )
}

export default CitasCrear;