import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Navbar from "../../componentes/Navbar";
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";
import SidebarContainer from "../../componentes/SidebarContainer";
import Footer from "../../componentes/Footer";

const PersonasCrear = () => {

  const navigate = useNavigate();

  const [persona, setPersonas] = useState({
    nombre:''
  });

  const { nombre} = persona;

  useEffect(() =>{
    document.getElementById("nombre").focus()
  })

  const onChange = (e) => {
    setPersonas({
      ...persona,
      [e.target.name]:e.target.value
    })
  }

  const crearPersona = async() => {
    const data = {
      nombre: persona.nombre
    }

    const response = await APIInvoke.invokePOST(`/api/personas`, data);
    
    console.log("Response",response)
    
const idPersona = response._id;

    if (idPersona === ''){
      const msg = "No se creo ninguna persona"
      swal({
        title: 'Error',
        text: msg,
        icon: 'error',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-danger',
          closeModal: true}
        }
      })
    }else{
      navigate("/personas-admin")
      const msg = "Esta persona fue creada correctamente"
      swal({
        title: 'Informacion',
        text: msg,
        icon: 'success',
        buttons:{
          confirm:{
          text: 'Ok',
          value: true,
          visible: true,
          className: 'btn btn-primary',
          closeModal: true}
        }
      })
      setPersonas({nombre:''})
    }
  }

  
  const onSubmit = (e) => {
    e.preventDefault();
    console.log("onsubmit", e)
    crearPersona();
  }

    return(
        <div className="wrapper">
      <Navbar></Navbar>
      <SidebarContainer></SidebarContainer>
      <div className="content-wrapper">
        <ContentHeader
          titulo={"Creacion Personas"}
          breadCrumb1={"Inicio"}
          breadCrumb2={"Personas"}
          breadCrumb3={"Citas"}
          breadCrumb4={"Tratamientos"}

          ruta1={"/home"}
          ruta3={"/citas-admin"}
          ruta4={"/tratamientos-admin"}
        />
        {/* </ContentHeader> */}
        <section className="content">
          <div className="card">
            <div className="card-header">
             
              <div className="card-tools">
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="collapse"
                  title="Collapse"
                >
                  <i className="fas fa-minus" />
                </button>
                <button
                  type="button"
                  className="btn btn-tool"
                  data-card-widget="remove"
                  title="Remove"
                >
                  <i className="fas fa-times" />
                </button>
              </div>
            </div>
            {/* 
              
                <div className="card-header">
                  <h3 className="card-title">Bordered Table</h3>
                </div> */}
                {/* /.card-header */}
                <div className="card-body">
                    <form onSubmit={onSubmit}>
                        <div className="card-body">
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre

                                </label>
                                <input type={"text"} className="form-control" id="nombre"
                                name="nombre" placeholder="Escriba el nombre de la persona"
                                value={nombre} onChange ={onChange} required>
                                </input>
                                
                            </div>
                        </div>
                        <div>
                          <button type="submit" className="btn btn-primary">
                            Crear
                          </button>
                        </div>
                    </form>
                </div>
                {/* /.card-body */}
                <div className="card-footer clearfix">
                  <ul className="pagination pagination-sm m-0 float-right">
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        «
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        1
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        2
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        3
                      </Link>
                    </li>
                    <li className="page-item">
                      <Link className="page-link" href="#">
                        »
                      </Link>
                    </li>
                  </ul>
                </div>
              
            

            {/* /.card-body */}
            {/* <div className="card-footer">Footer</div> */}
            {/* /.card-footer*/}
          </div>
        </section>
      </div>
      <Footer></Footer>
    </div>
    )
}

export default PersonasCrear;