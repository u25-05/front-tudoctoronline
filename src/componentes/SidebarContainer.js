import React from "react";
import Menu from "./Menu";
import Logo from '../../node_modules/admin-lte/dist/img/AdminLTELogo.png';
import { Link } from 'react-router-dom';

const SidebarContainer = () => {
    return (
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
            <Link to={"/home"} className="brand-link">
                &nbsp;&nbsp;&nbsp;
                <img src={"https://www.pngall.com/wp-content/uploads/2018/05/Doctor-PNG-Image-HD.png"}
                    alt="logo"
                    style={{ width: '20%', minWidth: '20%'}} />
                    &nbsp;&nbsp;&nbsp;
                <span className="brand-text font-weight-light">Tu Doctor Online</span>
            </Link>
            <div className="sidebar">
                <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="info">
                        &nbsp;&nbsp;
                    </div>
                    <div className="info">
                        <Link to={"/home"} className="d-block">Menu Principal</Link>
                    </div>
                </div>
                <Menu></Menu>
            </div>
        </aside>

    );
}

export default SidebarContainer;